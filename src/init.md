# 1. Record architecture decisions

## Status

Accepted

* Date: DATE
* Decision Makers:  Yahara Software

## Context / Problem Statement

We need to record the architectural decisions made on this project.

## Decision Outcome

We will use Architecture Decision Records, per Yahara standards as [described here](https://bitbucket.org/yaharasoftware/adr-tools/src/master/).

This project should reference and conform to our complete set of standard global architectural decisions found in the [Yahara global-adr repository](https://bitbucket.org/yaharasoftware/global-adr/src/master/).

## References

For additional background on ADRs, see the following background links:

* [Getting Started with Architecture Decision Records](https://ardalis.com/getting-started-with-architecture-decision-records/)
* [Michael Nygard's article, Documenting Architectural Decisions](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions)
* [Markdown Architectural Decision Records](https://adr.github.io/madr/)
