# NUMBER. TITLE

## Status

STATUS
<!-- Please remove this comment. Accepted statuses [Proposed | Rejected | Accepted | Deprecated | superseded by ADR-0005]. A new ADR should begin as Proposed -->
* Date: DATE
* Decision Makers:  [list everyone involved in the decision]

## Context / Problem Statement

[Describe the context and problem statement, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.]

## Decision Drivers

* [driver 1, e.g., a force, facing concern, …]
* [driver 2, e.g., a force, facing concern, …]
* … <!-- numbers of drivers can vary -->

## Considered Options

* [option 1]
* [option 2]
* [option 3]
* … <!-- numbers of options can vary -->

## Decision Outcome

Chosen option: "[option 1]", because [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)].

## Positive Consequences

* [e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
* …

## Negative Consequences

* [e.g., compromising quality attribute, follow-up decisions required, …]
* …
