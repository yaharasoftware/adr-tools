Installation
============

Yahara
------------------

It is recommended to install the tools using the Linux subsystem options below

Windows 10
----------

Linux subsystem
---------------

The scripts work in the Bash on [Ubuntu on Windows](https://www.microsoft.com/store/p/ubuntu/9nblggh4msv6), the Linux-subsystem that officially supports Linux command line tools.
Make sure that you have [installed](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide) the Linux-subsystem, run `bash` on the command line and follow the instructions in the "From Git (Linux, MacOS X)" section below.

From Git (Linux, MacOS X)
-------------------------

You can install with Git, if you want to be on the bleeding edge:

1. Clone this repository `git clone https://adamsysadmin@bitbucket.org/yaharasoftware/adr-tools.git`
2. Add the `src/` subdirectory to your PATH.

The following commands will get the latest source and add the path to .bashrc so it is available in future sessions

```
cd ~	
git clone https://git@bitbucket.org/yaharasoftware/adr-tools.git
echo -e 'PATH=$PATH:~/adr-tools/src' >> ~/.bashrc
source ~/.bashrc
```

Git for Windows: git bash (unsupported)
-------------------------

When using git bash within [Git for Windows](https://git-for-windows.github.io/), the scripts can simply be put in `usr\bin` in the installation directory.  That directory usually is `C:\Program Files\Git\usr\bin`.

1. Clone this repository `git clone https://git@bitbucket.org/yaharasoftware/adr-tools.git`
3. Copy everything from `src/` into `C:\Program Files\Git\usr\bin`
4. Pottery expects to run in a standard POSIX environment, so you must also install `more` or set the `PAGER` environment variable to `less`.

Autocomplete
----------

In order to have autocomplete on the commands, add the `autocomplete/adr` script to your `/etc/bash_autocomplete.d` or the equivalent to your platform.